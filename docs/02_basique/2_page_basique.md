---
author: Mireille Coilhac et Vincent-Xavier Jumel
title: Syntaxes de base
---


!!! danger "Attention aux indentations"

    Les indentations de 4 espaces peuvent être réaisées dans le Web IDE en utilisant la touche de tabulation
     à condition d'avoir réalisé le réglage du Web IDE "Select indentation".
     
    👉 [Configuration du Web IDE](../10_survie/kit_gitlab.md){:target="_blank" }


## I. Syntaxe basique de Markdown

!!! info "Les titres"

    ```markdown title="Le code à copier"
    ## Titre 1 : Mon titre de niveau 1

    Mon paragraphe

    ### Titre 2 : Mon titre de niveau 2
    ```
    <div class="result" markdown>
    ## Titre 1 : Mon titre de niveau 1

    Mon paragraphe 1

    ### Titre 2 : Mon titre de niveau 2

    Mon paragraphe 2
    </div>

### 2. Les tableaux

Il est facile de réaliser des tableaux avec Markdown, mais la présentation est basique.  
Les largeurs de colonnes sont gérées automatiquement, les espaces entre les `|  |` peuvent être aléatoires.  
Il faut laisser une ligne vide avant de commencer un tableau.


!!! info "Les tableaux"

    ```markdown title="Le code à copier"
    | Titre 1  | Titre 2 | Titre 3 |
    | :---    | :----:    | ---:   |
    | aligné gauche   | centré  | aligné droite |
    | Pomme | Brocoli | Pois |
    ```
    <div class="result" markdown>

    | Titre 1  | Titre 2 | Titre 3 |
    | :---    | :----:    | ---:   |
    | aligné gauche   | centré  | aligné droite |
    | Pomme | Brocoli | Pois |
    
    </div>

Pour des tableaux plus sophistiqués, on peut les écrire en HTML : 


!!! info "Les tableaux en HTML"
    ```html title="Le code à copier"
    <table>
    <tr>
        <td style="border:1px solid; font-weight:bold;">Poires</td>
        <td style="border:1px solid; font-weight:bold;">Pommes</td>
        <td style="border:1px solid; font-weight:bold;">Oranges</td>
        <td style="border:1px solid; font-weight:bold;">Kiwis</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    <tr>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">2 kg</td>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">500 g</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    </table>
    ```
    <div class="result" markdown>
    <table>
    <tr>
        <td style="border:1px solid; font-weight:bold;">Poires</td>
        <td style="border:1px solid; font-weight:bold;">Pommes</td>
        <td style="border:1px solid; font-weight:bold;">Oranges</td>
        <td style="border:1px solid; font-weight:bold;">Kiwis</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    <tr>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">2 kg</td>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">500 g</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    </table>
    </div>


### 3. Emojis qui peuvent être utiles


🌐 
👉
⚠️
🌵
💡
🤔
🌴
😂
🤿
😀
✏️
📝
😢
🐘
🖐️
👓
👗
👖
⌛
😊
🖲️
🏳️
😴
💻
👀

### 4. Insérer une image

A partir de Web IDE : Créer un dossier `images`. Télécharger dans ce dossier les images.

!!! info "Image à gauche"

    ```markdown title="Le code à copier"
    ![nom image](images/paysage_reduit.jpg){ width=20% }

    Par défaut l'image est placée à gauche.
    ```
    <div class="result" markdown>

    ![nom image](images/paysage_reduit.jpg){ width=20% }

    Par défaut l'image est placée à gauche.

    </div>


!!! info "Image à droite"

    ```markdown title="Aligner une image à droite"
    ![](images/paysage_reduit.jpg){ width=10%; align=right }

    Le texte se place **à gauche** de mon image placée à droite.
    ```
    <div class="result" markdown>

    ![](images/paysage_reduit.jpg){ width=10%; align=right }

    Le texte se place **à gauche** de mon image placée à droite.

    </div>

!!! info "3 images côtes à côtes"

    ```markdown title="Le code à copier"
    ![](images/paysage_reduit.jpg){ width=5% }
    ![](images/paysage_reduit.jpg){ width=10% }
    ![](images/paysage_reduit.jpg){ width=15% }
    ```
    <div class="result" markdown>

    ![](images/paysage_reduit.jpg){ width=5% }
    ![](images/paysage_reduit.jpg){ width=10% }
    ![](images/paysage_reduit.jpg){ width=15% }

    </div>

 
!!! info "Image centrée"

    ```markdown title="Le code à copier"
    ![paysage](images/paysage_reduit.jpg){ width=10%; : .center }

    Le texte se place **en dessous** de l'image centrée.
    ```
    <div class="result" markdown>
    ![paysage](images/paysage_reduit.jpg){ width=10%; : .center }

    Le texte se place **en dessous** de l'image centrée.
    </div>

### 5. Ecriture de code

!!! info "Les backticks"

    Nous utilisons pour cela un ou trois **backticks** (ou **apostrophe inversée**). Attention, à ne pas le confondre un **backtick** avec un guillemet. On le trouve généralement avec les touche <kbd>ALT GR</kbd> + <kbd>è</kbd> du clavier.


!!! info "Code au fil d'un texte inline"

    ```markdown title="Le code à copier"
    La variable `nombres` est de type `list`.
    ```
    <div class="result" markdown>
    La variable `nombres` est de type `list`.
    </div>


  
### 6. Les touches du clavier


!!! info "Une première méthode pour faire apparaître des touches du clavier"

    ```markdown title="Le code à copier"
    Nous allons utiliser la touche <kbd>%</kbd>
    ```
    <div class="result" markdown>
    Nous allons utiliser la touche <kbd>%</kbd>
    </div>

!!! info "Une deuxième méthode pour faire apparaître des touches du clavier"

    On peut trouver beaucoup de touches ici : [Touches du clavier](https://facelessuser.github.io/pymdown-extensions/extensions/keys/#overview){:target="_blank" }

    Voici des exemples d'utilisation :

    ```markdown title="Le code à copier"
    ++ctrl+alt+del++

    ++enter++
    ```
    <div class="result" markdown>
    ++ctrl+alt+del++

    ++enter++
    </div>


### 7. Des "trous dans le texte"

!!! info "Les espaces"

    Pour faire apparaître des espaces dans un texte, mettre des espaces ne suffit pas .

    ```markdown title="Le code avec espace"
    Par exemple : Dans la phrase "Je lis un tutoriel." Le verbe "lis" est au                de l'indicatif.
    ```
    <div class="result" markdown>
    Par exemple : Dans la phrase "Je lis un tutoriel." Le verbe "lis" est au                de l'indicatif.
    </div>

!!! info "une possibilité pour ajouter des espaces"

    ```markdown title="Le code avec espace"
    Par exemple : Dans la phrase "Je lis un tutoriel." Le verbe "lis" est au $\hspace{10em}$ de l'indicatif.
    ```

    <div class="result" markdown>
    Par exemple : Dans la phrase "Je lis un tutoriel." Le verbe "lis" est au $\hspace{10em}$ de l'indicatif.
    </div>


## II. Les admonitions

Toutes les admonitions ont un titre par défaut. Pour personnaliser un titre, il suffit de le rajouter entre guillemets.

!!! info "Admonition info"
    ````markdown title="Le code à copier"
    !!! info "Mon info"
        
        Ma belle info qui doit être indentée
    ````
    <div class="result" markdown>
    !!! info "Mon info"
        
        Ma belle info qui doit être indentée
    </div>


!!! info "Admonition remarque"

    ```markdown title="Le code à copier"
    !!! warning "Remarque"

        Ma remarque qui doit être indentée
    ```
    <div class="result" markdown>
    !!! warning "Remarque"

        Ma remarque qui doit être indentée
    </div>


!!! info "Admonition attention"
    ```markdown title="Le code à copier"
    !!! danger "Attention"

        texte indenté   
    ```
    <div class="result" markdown>
    !!! danger "Attention"

        texte indenté
    </div>


!!! info "Admonition note à dérouler"
    ```markdown title="Le code à copier"
    ??? note "se déroule en cliquant dessus"

        Ma note indentée
    ```
    <div class="result" markdown>
    ??? note "se déroule en cliquant dessus"

        Ma note indentée
    </div>


!!! info "Admonition astuce à dérouler"
    ```markdown title="Le code à copier"
    ??? tip "Astuce"

        Ma belle astuce indentée
    ```
    <div class="result" markdown>
    ??? tip "Astuce"

        Ma belle astuce indentée
    </div>


!!! info "Admonition résumé"
    ```markdown title="Le code à copier"
    !!! abstract "Résumé"

        Mon bilan indenté
    ```
    <div class="result" markdown>
    !!! abstract "Résumé"

        Mon bilan indenté 
    </div>


!!! info "Admonition note repliable dépliée"
    ```markdown title="Le code à copier"
    ???+ note dépliée

        Mon texte indenté
    ```
    <div class="result" markdown>
    ???+ note dépliée

        Mon texte indenté
    </div>


!!! info "Admonition note à cliquer"
    ```markdown title="Le code à copier"
    ??? note pliée "Note à cliquer"

        Mon texte indenté qui se dévoilera après le clic  
    ```
    <div class="result" markdown>
    ??? note pliée "Note à cliquer"

        Mon texte indenté qui se dévoilera après le clic
    </div>


!!! info "Admonition échec"
    ```markdown title="Le code à copier"
    !!! failure "Echec"

        Mon texte indenté
    ```
    <div class="result" markdown>
    !!! failure "Echec"

        Mon texte indenté
    </div>


!!! info "Admonition bug"
    ```markdown title="Le code à copier"
    !!! bug "Bug"

        Mon texte indenté
    ```
    <div class="result" markdown>
    !!! bug
        
        Mon texte indenté
    </div>


!!! info "Admonition exemple"
    ```markdown title="Le code à copier"
    !!! example "Exemple"

        Mon exemple indenté
    ```
    <div class="result" markdown>
    !!! example "Exemple"

        Mon exemple indenté
    </div>


!!! info "Admonition note dans la marge gauche"
    ```markdown title="Le code à copier"
    !!! note inline "Note à gauche"

        Texte de la note indenté
        
    Le texte non indenté se place à droite de la note.  
    Il est en dehors de l'admonition.  
    Il doit être assez long  
    ...
    ```
    <div class="result" markdown>
    !!! note inline "Note à gauche"

        Texte de la note indenté
        
    Le texte non indenté se place à droite de la note.  
    Il est en dehors de l'admonition.  
    Il doit être assez long  
    ...
    </div>


!!! info "Admonition note dans la marge droite"
    ```markdown title="Le code à copier"

    !!! note inline end "Note à droite"

        Texte de la note indenté
        
    Le texte non indenté se place à droite de la note.  
    Il est en dehors de l'admonition.  
    Il doit être assez long  
    ...
    ```
    <div class="result" markdown>
    !!! note inline end "Note à droite"

        Texte de la note indenté
    
    Le texte non indenté se place à droite de la note.  
    Il est en dehors de l'admonition.  
    Il doit être assez long  
    ...
    </div>


!!! info "Admonition exercice"

    ```markdown title="Le code à copier"
    ???+ question "Mon exercice"

        **1.** Question 1.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 1.
            Le texte doit être indenté dans cette nouvelle admonition

        **2.** Question 2.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 2.
            Le texte doit être indenté dans cette nouvelle admonition
    ```
    <div class="result" markdown>

    ???+ question "Mon exercice"

        **1.** Question 1.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 1.
            Le texte doit être indenté dans cette nouvelle admonition

        **2.** Question 2.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 2.
            Le texte doit être indenté dans cette nouvelle admonition

    </div>


!!! info "Panneaux coulissants"

    ```markdown title="Le code à copier"
    !!! example "Exemples avec trois panneaux"

        === "Panneau 1"
            Ici du texte concernant ce panneau 1

            Il peut prendre plusieurs lignes

        === "Panneau 2"
            Ici du texte concernant ce panneau 2

            Il peut prendre plusieurs lignes

        === "Panneau 3"
            Ici du texte concernant ce panneau 3

            Il peut prendre plusieurs lignes

            On peut aussi mettre une image : 

            ![nom image](images/paysage_reduit.jpg){ width=20% }
    ```  
    <div class="result" markdown>

    !!! example "Exemples avec trois panneaux"

        === "Panneau 1"
            Ici du texte concernant ce panneau 1

            Il peut prendre plusieurs lignes

        === "Panneau 2"
            Ici du texte concernant ce panneau 2

            Il peut prendre plusieurs lignes

        === "Panneau 3"
            Ici du texte concernant ce panneau 3

            Il peut prendre plusieurs lignes

            On peut aussi mettre une image : 

            ![nom image](images/paysage_reduit.jpg){ width=20% }


    </div>




## III. Faire des liens 

Ce qui est entre crochets comme `[link text]` est personnalisable.

### 1. Liens externes

!!! info "Lien externe sans bouton"

    ```markdown title="Le code à copier"
    [Additionner des fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&v=eleve&es=021100&title=)
    ```

    <div class="result" markdown>

    [Additionner des fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&v=eleve&es=021100&title=)

    </div>

!!! info "Lien externe sans bouton dans une autre fenêtre"

    ```markdown title="Le code à copier"
    [Additionner des fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&v=eleve&es=021100&title=){:target="_blank" }
    ```

    <div class="result" markdown>

    [Additionner des fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&v=eleve&es=021100&title=){:target="_blank" }

    </div>



!!! info "Lien externe avec bouton dans une autre fenêtre"

    ```markdown title="Le code à copier"
    [Additionner des fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&v=eleve&es=021100&title=){ .md-button target="_blank" rel="noopener" }
    ```

    <div class="result" markdown>

    [Additionner des fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&v=eleve&es=021100&title=){ .md-button target="_blank" rel="noopener" }

    </div>

### 2. Liens internes

!!! info "Lien vers un article du **même** répertoire"

    Lien vers le fichier `3_mon_article.md` du **même** répertoire.
 
    ```markdown title="Le code à copier"
    [Mon article](3_mon_article.md)
    ```
    <div class="result" markdown>

    [Mon article](3_mon_article.md)

    </div>

!!! info "Lien vers un article du **répertoire parent** du répertoire courant"

    Lien vers le fichier `ajouts.md`.

    ```markdown title="Le code à copier"
    [Ajouts](../ajouts.md)
    ```
    <div class="result" markdown>

    [Ajouts](../ajouts.md)

    </div>

!!! info "Lien vers un article d’un **sous-répertoire** du répertoire courant"

    Lien vers le fichier `mon_article.md` du sous-répertoire `directory`. (ne fonctionne pas sur ce site)

    ```markdown title="Le code à copier"
    [link text](directory/mon_article.md)
    ```
    <div class="result" markdown>

    [link text](directory/mon_article.md)

    </div>



!!! info "Lien vers un article d’un **sous-répertoire du répertoire parent du répertoire actif**"

    Lien vers le fichier `erreurs_frequentes.md` du sous-répertoire `erreurs` du répertoire parent du répertoire actif.


    ```markdown title="Le code à copier"
    [FAQ et erreurs fréquentes](../erreurs/erreurs_frequentes.md)
    ```
    <div class="result" markdown>

    [FAQ et erreurs fréquentes](../erreurs/erreurs_frequentes.md)

    </div>


### 3. Liens de téléchargements de fichiers

Mettre le fichier `file_telecharger` dans un dossier `a_telecharger`. Bien indiquer le bon chemin comme dans l'exemple ci-dessous

!!! info "Mon fichier à donner en téléchargement"

    ```markdown title="Le code à copier"
    
    🌐 Fichier à télécharger :

    Fichier `file_telecharger` : [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/file_telecharger)

    ```
    <div class="result" markdown>

    🌐 Fichier à télécharger :
    
    Fichier `file_telecharger` : [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/file_telecharger)

    </div>



## IV. Insertion de vidéo

!!! info "Insérer une vidéo en ligne"

    ```markdown title="Le code à copier"
    <video controls src="https: ..."></video>
    ```

    Exemple : 

    ```markdown title="Le code à copier"
    <video controls src="https://upload.wikimedia.org/wikipedia/commons/d/d2/See_a_Koala_scratch%2C_yawn_and_sleep.webm"></video>

    _Source : Paolo Leone: https://www.youtube.com/channel/UCFRQrtHw36NNpX1oXpHMfRw, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons_
    ```

    <div class="result" markdown>
    <video controls src="https://upload.wikimedia.org/wikipedia/commons/d/d2/See_a_Koala_scratch%2C_yawn_and_sleep.webm"></video>
    
    _Source : Paolo Leone: https://www.youtube.com/channel/UCFRQrtHw36NNpX1oXpHMfRw, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons_
    </div>

👉 Il suffit le plus souvent de recopier le code qui est proposé dans "partager" et "intégrer" de la vidéo que l'on veut mettre.

!!!example "Exemple d'une vidéo de Portail tubes de apps.education.fr"
    ```html
    <iframe title="Présentation de CodiMD (et du code Markdown) en moins de 2 minutes" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/5a1256f5-a3cd-4798-84f9-3dd7b8f01aac" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    ```
    <div class="result" markdown>
    <iframe title="Présentation de CodiMD (et du code Markdown) en moins de 2 minutes" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/5a1256f5-a3cd-4798-84f9-3dd7b8f01aac" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    </div>

   

## V. QCM

!!! info "Barrer des réponses fausses"

    Mettre un texte entre ~~ et ~~ permet de le barrer.

!!! info "Créer un QCM avec des volets"

    ```markdown title="Le code à copier"
    ???+ question

        Voici diverses propositions

        === "Cocher la ou les affirmations correctes"
                
            - [ ] Proposition 1
            - [ ] Proposition 2
            - [ ] Proposition 3
            - [ ] Proposition 4

        === "Solution"
                
            - :x: ~~Proposition 1~~ Optionnel : Faux car ... 
            - :white_check_mark: Proposition 2 . Optionnel : Juste car ...
            - :white_check_mark: Proposition 3 . Optionnel : Juste car ...
            - :x: ~~Proposition 4~~ Optionnel : Faux car ... 
    ```
    <div class="result" markdown>

    ???+ question

        Voici diverses propositions

        === "Cocher la ou les affirmations correctes"
                
            - [ ] Proposition 1
            - [ ] Proposition 2
            - [ ] Proposition 3
            - [ ] Proposition 4

        === "Solution"
                
            - :x: ~~Proposition 1~~ Optionnel : Faux car ... 
            - :white_check_mark: Proposition 2 . Optionnel : Juste car ...
            - :white_check_mark: Proposition 3 . Optionnel : Juste car ...
            - :x: ~~Proposition 4~~ Optionnel : Faux car ... 

    </div>



!!! info "Créer un QCM avec validation, rechargement et score : un premier exemple"


    ```markdown title="Le code à copier"
    {% raw %}
    {{ multi_qcm(
    ["Compléter : $2^3=$", ["9", "8", "Je ne sais pas", "6"],[2],],
    multi = False,
    qcm_title = "Puissances",
    shuffle = True,
    )  }}
    {% endraw %}
    ```

    <div class="result" markdown>

    {{ multi_qcm(
    ["Compléter : $2^3=$", ["9", "8", "Je ne sais pas", "6"],[2],],
    multi = False,
    qcm_title = "Puissances",
    shuffle = True,
    ) }}

    </div>

!!! info "Le principe"

    Chaque question est une liste contenant : l'énoncé, une liste des réponses à choisir, 
    une liste des réponses justes numérotées à partir de 1, et éventuellement la donnée du fait qu'il y ait plusieurs réposes possibles ou pas.

    Après les questions on trouve des options de réglage du QCM complet. `shuffle = True` mélange toutes les questions et toutes les réponses proposées à chaque rechargement du QCM.

    !!! warning "Un piège avec les guillemets"

        Toutes les questions et toutes les réposes sont des chaines de caractères entre `" ... "`. ou entre `""" ... """` si elles prennent plusieurs lignes.

        Il ne faut donc **absolument pas** utiliser de `"` dans les questions et réponses. 
        
        Pour utiliser des guillemets, il faudra **absolument** utiliser des guillemets à simple côte : **`'`**

    !!! warning "Un piège avec les formules de maths"

        Les opérateurs LaTex utilisant un échappement de caractère devront être eux-mêmes échappés : `"$3 \\times 5$"`

    !!! warning "Admonition"

        Il est inutile de mettre le QCM dans une admonition, car cela se fait automatiquement : voir le rendu ci-dessous.


!!! info "Exemple avec des énoncés ou réponses multi-lignes proposé par frédéric Zinelli"


    ````markdown title="Le code à copier"
    {% raw %}{{{% endraw %} multi_qcm(
        [
    """
    On a saisi le code suivant :
    ```python title=''
    n = 8
    while n > 1:
        n = n/2
    ```

    Que vaut `n` après l'exécution du code ?
    """,
            [
                "2.0",
                "4.0",
                "1.0",
                "0.5",
            ],
            [3]
        ],
        [
            "Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?",
            [
                "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
                "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
                "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
                "Le serveur web sur lequel est stockée la page HTML."
            ],
            [1],
        ],
        [
            """
        Quelle expression permet d'accéder au numéro de Tournesol :
        ```python title=''
        repertoire = [{'nom': 'Dupont', 'tel': 5234}, {'nom': 'Tournesol', 'tel': 5248}, {'nom': 'Dupond', 'tel': 5237}]
        ```
            """,
            [
                "`#!py repertoire[1]['tel']`",
                "`#!py repertoire['tel'][1]`",
                "`#!py repertoire['Tournesol']`",
                "`#!py repertoire['Tournesol']['tel']`",
            ],
            [1],
            {'multi':True}
        ],
        multi = False,
        qcm_title = "Un QCM avec mélange automatique des questions (bouton en bas pour recommencer)",
        DEBUG = False,
        shuffle = True,
    ) {% raw %}}}{% endraw %}
    ````


    <div class="result" markdown>

    {{ multi_qcm(
        [
    """
    On a saisi le code suivant :
    ```python title=''
    n = 8
    while n > 1:
        n = n/2
    ```

    Que vaut `n` après l'exécution du code ?
    """,
            [
                "2.0",
                "4.0",
                "1.0",
                "0.5",
            ],
            [3]
        ],
        [
            "Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?",
            [
                "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
                "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
                "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
                "Le serveur web sur lequel est stockée la page HTML."
            ],
            [1],
        ],
        [
            """
        Quelle expression permet d'accéder au numéro de Tournesol :
        ```python title=''
        repertoire = [{'nom': 'Dupont', 'tel': 5234}, {'nom': 'Tournesol', 'tel': 5248}, {'nom': 'Dupond', 'tel': 5237}]
        ```
            """,
            [
                "`#!py repertoire[1]['tel']`",
                "`#!py repertoire['tel'][1]`",
                "`#!py repertoire['Tournesol']`",
                "`#!py repertoire['Tournesol']['tel']`",
            ],
            [1],
            {'multi':True}
        ],
        multi = False,
        qcm_title = "Un QCM avec mélange automatique des questions (bouton en bas pour recommencer)",
        DEBUG = False,
        shuffle = True,
    ) }}

    </div>


!!! info "Tout sur les QCM"

    [Documentation officielle pour les QCM](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/qcms/){ .md-button target="_blank" rel="noopener" }



## VI. Exercices en lignes

!!! info "Des exercices en ligne"

    Il existe beaucoup d'exercices en ligne, il suffit de donner le lien (par exemple  [MathALEA : générer des exercices de maths](https://coopmaths.fr/alea/){:target="_blank" } )


!!! info "Bouton de lien vers un exercice"

    ```markdown title="Le code à copier"
    ???+ question "Les fractions"

        Manipuler les fractions :

        [Additionner ou soustraire des fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&alea=vzyN&v=eleve&es=021100){ .md-button target="_blank" rel="noopener" }
    ```
    <div class="result" markdown>

    ???+ question "Les fractions"

        Manipuler les fractions :

        [Additionner ou soustraire des fractions](https://coopmaths.fr/alea/?uuid=b51ec&id=2N30-2&alea=vzyN&v=eleve&es=021100){ .md-button target="_blank" rel="noopener" }

    </div>




## VII. Les arbres 

Il suffit de numéroter les noeuds : n0, n1, n2 etc. avec les étiquettes entre parenthèses, et les flèches pour les arcs entre les noeuds.
En 1er à gauche.

!!! info "Un arbre"

    ````markdown title="Le code à copier"
    ```mermaid
        %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
        flowchart TB
            n0(15) --> n1(6)
            n1 --> n3(1)
            n1 --> n4(10)
            n0 --> n2(30)
            n2 --> n8[Null]
            n2 --> n5(18)
            n5 --> n6(16)
            n5 --> n7(25)
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
        flowchart TB
            n0(15) --> n1(6)
            n1 --> n3(1)
            n1 --> n4(10)
            n0 --> n2(30)
            n2 --> n8[Null]
            n2 --> n5(18)
            n5 --> n6(16)
            n5 --> n7(25)
    ```
    </div>


!!! info "Arbres avec certains nœuds absents "

    Voir l'exemple qui suit :

    * On numérote l'arc à ne pas représenter (le 1er est le numéro 0), et on utilise par exemple pour le numéro 3 la syntaxe :  
    `linkStyle 3 stroke-width:0px;`
    * On met les nœuds vides avec `opacity:0` :  
    `style E opacity:0;`
    

!!! info "Des nœuds absents mais leur espace conservé"

    ````markdown title="Le code à copier"
    ```mermaid
    flowchart TD
        A(12) --- B(10)
        A --- C(15)
        B --- D(5)
        B --- E( )
        D --- F(4)
        D --- G(8)
        C --- H( )
        C --- I(20)
        linkStyle 3 stroke-width:0px;
        linkStyle 6 stroke-width:0px;
        style E opacity:0;
        style H opacity:0;
    ```
    ````
    <div class="result" markdown>

    ```mermaid
    flowchart TD
        A(12) --- B(10)
        A --- C(15)
        B --- D(5)
        B --- E( )
        D --- F(4)
        D --- G(8)
        C --- H( )
        C --- I(20)
        linkStyle 3 stroke-width:0px;
        linkStyle 6 stroke-width:0px;
        style E opacity:0;
        style H opacity:0;
    ```
    </div>

!!! info "Placement des nœuds définis"

    Dans le cas des arbres binaires de recherche par exemple, on a besoin que les nœuds soient représentés dans un ordre bien précis.
    Il suffit pour cela de les donner dans le code mermaid dans l'ordre de ce qui serait le parcours en largeur de l'arbre (par niveau).

    ````markdown title="Le code à copier"
    ```mermaid
    graph TD
    A(5)
    B(2)
    C(8)
    F( )
    G( )
    D(6)
    E(9)
    H( )
    I( )
    J( )
    K( )
    A --> B
    A --> C
    C --> D
    C --> E
    B --> F
    B --> G
    D --> H
    D --> I
    E --> J
    E --> K
    ```
    ````
    <div class="result" markdown>

    ```mermaid
    graph TD
    A(5)
    B(2)
    C(8)
    F( )
    G( )
    D(6)
    E(9)
    H( )
    I( )
    J( )
    K( )
    A --> B
    A --> C
    C --> D
    C --> E
    B --> F
    B --> G
    D --> H
    D --> I
    E --> J
    E --> K
    ```
    </div>


[Lien vers différentes options de nœuds et flèches](https://coda.io/@leandro-zubrezki/diagrams-and-visualizations-using-mermaid/flowcharts-3){ .md-button target="_blank" rel="noopener" }


[lien vers la documentation mermaid - essais en ligne de mermaid](https://mermaid.live/edit#pako:eNpVkE1qw0AMha8itGohvoAXgcZus0lJodl5vBAeOTOk88NYpgTbd-84ptBqJfS-J6Q3YRc0Y4nXRNHApVYecr00lUl2EEdDC0Wxn48s4ILn-wyHp2OAwYQYrb8-b_xhhaCaTivGIMb627JJ1cN_9jxD3ZwoSojtX-XyHWZ4beyHyev_KyZxdr01PZU9FR0lqCi1uEPHyZHV-expNSgUw44VlrnV3NP4JQqVXzJKo4TPu--wlDTyDseoSbi2lB92v0PWVkJ635J4BLL8AM-3Wn4){ .md-button target="_blank" rel="noopener" }

!!! info "Un arbre horizontal"

    ````markdown title="Le code à copier"
    ```mermaid
    graph LR
	    A( )
	    B(0)
	    C(1)
	    D(0)
	    E(1)
	    F(0)
	    G(1)
	    A --- B
	    A --- C
	    B --- D
	    B --- E
	    C --- F
	    C --- G
    ```
    ````

    <div class="result" markdown>

    ```mermaid
    graph LR
	    A( )
	    B(0)
	    C(1)
	    D(0)
	    E(1)
	    F(0)
	    G(1)
	    A --- B
	    A --- C
	    B --- D
	    B --- E
	    C --- F
	    C --- G
    ```
    </div>


## VIII. Les graphes

!!! info "graphe orienté"
    ````markdown title="Le code à copier"
    ``` mermaid
        graph LR
        A[texte 1] ----> |50| C[texte 2];
        A -->|1| B[B];
    ```
    ````
    <div class="result" markdown>

    ``` mermaid
        graph LR
        A[texte 1] ----> |50| C[texte 2];
        A -->|1| B[B];
    ```
    </div>


!!! info "graphe orienté horizontal"

    ````markdown title="Le code à copier"
    ``` mermaid
        graph LR
            A[Editeur de texte PHP] --> C[Serveur Apache]
            C --> B[Navigateur]
    ```
    ````
    <div class="result" markdown>

    ``` mermaid
        graph LR
            A[Editeur de texte PHP] --> C[Serveur Apache]
            C --> B[Navigateur]
    ```
    </div>


!!! info "graphe non orienté"

    ````markdown title="Le code à copier"
    ``` mermaid
        graph LR
            A ---|1| B
            B ---|5| C
            A ---|5| D
            B ---|1| D
            D ---|1| C
            A ---|50| C
    ```
    ````
    <div class="result" markdown>

    ``` mermaid
        graph LR
            A ---|1| B
            B ---|5| C
            A ---|5| D
            B ---|1| D
            D ---|1| C
            A ---|50| C
    ```
    </div>

!!! info "graphe orienté avec des nœuds circulaires ou carrés"

    ````markdown title="Le code à copier"
    ```mermaid
        graph LR
            A[Fichier] --> D((D))
            D --> C[Clavier]
            C --> B((B))
            B --> A
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        graph LR
            A[Fichier] --> D((D))
            D --> C[Clavier]
            C --> B((B))
            B --> A
    ```
    </div>

[Lien vers différentes options de nœuds et flèches](https://coda.io/@leandro-zubrezki/diagrams-and-visualizations-using-mermaid/flowcharts-3){ .md-button target="_blank" rel="noopener" }

[lien vers la documentation mermaid - essais en ligne de mermaid](https://mermaid.live/edit#pako:eNpVkE1qw0AMha8itGohvoAXgcZus0lJodl5vBAeOTOk88NYpgTbd-84ptBqJfS-J6Q3YRc0Y4nXRNHApVYecr00lUl2EEdDC0Wxn48s4ILn-wyHp2OAwYQYrb8-b_xhhaCaTivGIMb627JJ1cN_9jxD3ZwoSojtX-XyHWZ4beyHyev_KyZxdr01PZU9FR0lqCi1uEPHyZHV-expNSgUw44VlrnV3NP4JQqVXzJKo4TPu--wlDTyDseoSbi2lB92v0PWVkJ635J4BLL8AM-3Wn4){ .md-button target="_blank" rel="noopener" }





