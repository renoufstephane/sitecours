---
author: Mireille Coilhac
title: Ajouts sur ce site
---

Voici les ajouts notables réaisés sur ce site depuis sa publication.

* 06/06/2024 : comment cacher un répertoire dans le menu de navigation
* 11/06/2024 : syntaxe simple pour insérer une vidéo [Insérer des vidéos](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-basique/02_basique/2_page_basique/#iv-insertion-de-video){:target="_blank" }
* 16/06/2024 : la  FAQ sur une page qui n'apparaît pas dans le menu
* 18/06/2024 : comment supprimer un commit (revert) [à la fin du V.](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/10_survie/kit_gitlab/#v-faire-un-commit-pour-un-nouveau-repertoire-le-telechargement-de-fichiers-la-creation-dun-fichier){:target="_blank" }
* 19/06/2024 : glisser-déposer des fichiers [Kit de survie GitLab](./10_survie/kit_gitlab.md){:target="_blank" }
* 19/06/2024 : voir les modifications d'un commit [Kit de survie GitLab](./10_survie/kit_gitlab.md){:target="_blank" }
* 19/06/2024 : visualiser le rendu du code markdown [Kit de survie GitLab](./10_survie/kit_gitlab.md){:target="_blank" }
* 20/06/2024 : formules en LaTeX pour les maths et la chimie [LaTeX](./latex/formules_latex.md){:target="_blank" }
* 01/07/2024 : un démarrage pas à pas : [Parcours pas à pas](./parcours/pas_a_pas.md){:target="_blank" }
* 25/08/2024 : mise à jour nécessaire suite à la mise à jour du thème PMT (passage à Pyodide MkDocs Theme v.2.2.0) [FAQ](./erreurs/erreurs_frequentes.md){:target="_blank" }




