---
author: Mireille Coilhac
title: Formules en mathématiques et chimie
---

!!! info "Utiliser LaTex"

    On peut intégrer des formules écrites en LaTeX, dans une ligne de texte, en la mettant entre `$...$` ou 
    centrée à la ligne suivante en les mettant entre `$$...$$`.

## En mathématiques


!!! info "Exemple sur plusieurs lignes"

    ````markdown title="Code à copier"
    {% raw %}
    $$
    \left\{
            \begin{array}{ll}
                u_0 = 3 \\
                u_{n+1} = 5 \times u_n+2\\
            \end{array}
    \right.
    $$
    {% endraw %}
    ````

    **Rendu**

    ---

    $$
    \left\{
            \begin{array}{ll}
                u_0 = 3 \\
                u_{n+1} = 5 \times u_n+2\\
            \end{array}
    \right.
    $$

!!! info "Des tableaux avec lignes et colonnes tracées et ajustées au contenu"

    ````markdown title="Code à copier"
    {% raw %}
    $$
    \begin{array}{|l|c|r|}
    \hline
    \text{colonne 1 alignée à gauche} & \text{colonne 2 centrée} & \text{colonne 3 alignée à droite} \\
    \hline
    1.1 & 1.2 & 1.3 \\
    2.1 & 2.2 & 2.3 \\
    \hline
    \end{array}
    $$
    {% endraw %}
    ````

    **Rendu**

    ---

    $$
    \begin{array}{|l|c|r|}
    \hline
    \text{colonne 1 alignée à gauche} & \text{colonne 2 centrée} & \text{colonne 3 alignée à droite} \\
    \hline
    1.1 & 1.2 & 1.3 \\
    2.1 & 2.2 & 2.3 \\
    \hline
    \end{array}
    $$


!!! info "Ajouter ses propres commandes"

    ````markdown title="Code à copier"
    {% raw %}
    $$
    \newcommand{\norm}[1]{\left\lVert#1\right\rVert}
    \norm{\vec{v_C}} = \frac{\sqrt{(x_D - x_C)^2 + (y_D - y_C)^2}}{\Delta t}
    $$

    La norme du vecteur ${\vec{u}}$ se note $\norm{\vec{u}}$.
    {% endraw %}
    ````

    **Rendu**

    ---

    $$
    \newcommand{\norm}[1]{\left\lVert#1\right\rVert}
    \norm{\vec{v_C}} = \frac{\sqrt{(x_D - x_C)^2 + (y_D - y_C)^2}}{\Delta t}
    $$

    La norme du vecteur ${\vec{u}}$ se note $\norm{\vec{u}}$.



!!! info "Une aide en ligne pour écrire les formules"

    [LaTeX en ligne](https://latex.codecogs.com/eqneditor/editor.php){ .md-button target="_blank" rel="noopener" }


## En chimie

!!! info "Exemple sur deux lignes"

    ````markdown title="Code à copier"
    {% raw %}
    $$
    {CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}
    $$

    $$
    ^{14}_{6}C  \rightarrow \ ^{14}_{7}N  + \  ^{ 0}_{-1}e^{-}
    $$
    {% endraw %}
    ````

    **Rendu**

    ---

    $$
    {CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}
    $$

    $$
    ^{14}_{6}C  \rightarrow \ ^{14}_{7}N  + \  ^{ 0}_{-1}e^{-}
    $$




!!! info "Exemple sur une seule ligne"

    ````markdown title="Code à copier"
    {% raw %}
    On peut tout mettre en ligne : d'abord cette formule ${CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}$ 
    puis celle-ci :  $^{14}_{6}C  \rightarrow \ ^{14}_{7}N  + \  ^{ 0}_{-1}e^{-}$
    {% endraw %}
    ````

    <div class="result" markdown>
    On peut tout mettre en ligne : d'abord cette formule ${CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}$ 
    puis celle-ci :  $^{14}_{6}C  \rightarrow \ ^{14}_{7}N  + \  ^{ 0}_{-1}e^{-}$
    </div>




