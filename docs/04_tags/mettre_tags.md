---
author: Mireille Coilhac
title: Ajouter des tags
---

## I. Dans votre dépôt

!!! info "A vérifier dans le dépôt"

    Si ce n'est pas déjà fait il faut : 

    **1. Créer une page tags.md, puis la mettre dans le dossier `docs`**

    !!! info "Exemple de fichier tags.md"

    	```markdown title="fichier tags.md"
    	# 🏷️ Tags

    	```


    **2. Modifier le fichier mkdocs.yml :**

    Sous `plugins`  ajouter si nécessaire : 

    ```text title="Dans le fichier mkdocs.yml sous plugins recopier :"
    - tags:
        tags_file: tags.md
    ```

## II. Les pages avec tags


Il suffit de compléter l'en-tête de la page `.md`


```markdown title="début du fichier .md à recopier"
---
author: Noms d'auteurs
title: Titre de la page
tags:
  - Nom du tag 
---

Suite de ma page ...

```

### Exemple dans "Chapitre 1" du site modèle


Dans le site modèle par exemple il y a les tags `mon tag 1` et `mon tag 1`: [Chapitre 1](https://docs.forge.apps.education.fr/modeles/site-web-cours-general/chapitre01/01cours/){:target="_blank" }

!!! info "Début du fichier 01cours.md pour la page Chapitre 1 "

    ````markdown title="Chapitre 1"
    ---
    author: Votre nom
    title: Chapitre 1
    tags:
      - mon tag 1
      - mon tag 2
    ---

    ## I. Paragraphe 1 :

    !!! info "Mon info"

        Ma belle info qui doit être indentée
    ````


## II. Les tags dans le menu de votre site

!!! info "Les tags dans le menu de votre site "

    * 🪄 Aucune autre manipulation  n'est nécessaire. Dans le menu, en cliquant sur Tags ([Tags dans le menu du site modèle](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/tags/){:target="_blank" }) tous les tags apparaîtront de façon automatique.

    * 😊 Si dans vos fichiers `.md` vous en rajoutez ou supprimez des tags, la page de tags du site sera **automatiquement** mise à jour.



